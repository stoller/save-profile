## Required Software

The following list contains all of the software you will need to install
Emulab. We recommend downloading all of it beforehand to avoid delays
during the install process. Contact us for any Emulab-specific packages
that are not yet publicly available.

#### FreeBSD (10.3) - \[590MB\]

FreeBSD is the Operating System for all Emulab servers. Only the first
CD will be required for a standard installation.  
**Note:** You will want to install the 64-bit version, ***NOT*** the the
32-bit version. Make sure that the hardware you are installing FreeBSD
on supports 64-bit OSes\!

Main page: <http://www.freebsd.org>

Download: <http://www.freebsd.org/releases/10.3R/announce.html>

#### Emulab Server Package tarball (10.3) - \[290MB\]

Download:
<http://www.emulab.net/downloads/FreeBSD-10.3-packages-64.tar.gz>

#### Emulab-tested Ports collection for (10.3) - \[55MB\]

Download: <http://www.emulab.net/downloads/FreeBSD-10.3-ports.tar.gz>

See the <http://www.emulab.net/downloads/ports-README-10.3.txt> for
details about how this ports tree differs from the standard 10.3
distribution.

#### Emulab source tree

Get a copy of the *emulab-devel* (**NOT** *emulab-stable*) source code
from the [Emulab Git repository](../GitRepository).


  - [Prev:](prerequisites.html)
    Prerequisites
  - [Next:](updating_about.html)
    About Upgrading
  - [Home](../InstallDocs "Main Page")
